<?php

function selecteerklant($nummer, $connection){
    if($nummer == null) {
        return array("AccountID" => 0, "voornaam" => "", "tussenvoegsel" => "", "achternaam" => "", "telefoonnummer" => "","emailadres" => "", "wachtwoord" => "", "straat" => "","huisnummer" => "","postcode" => "","woonplaats" => "", "land" => "");
    }
    $statement = mysqli_prepare($connection, "SELECT AccountID, voornaam, tussenvoegsel, achternaam, telefoonnummer, emailadres, wachtwoord, straat, huisnummer, postcode, woonplaats, land  FROM account where AccountID= ?");
    mysqli_stmt_bind_param($statement, 'i', $nummer);
    mysqli_stmt_execute($statement);
    $result = mysqli_stmt_get_result($statement);
    return mysqli_fetch_all($result, MYSQLI_ASSOC)[0];
}


