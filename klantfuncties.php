<?php
include 'databasefuncties.php';

function voegKlantToe($connection, $voornaam, $tussenvoegsel, $achternaam, $telefoonnummer, $emailadres, $wachtwoord, $straat, $huisnummer, $postcode, $woonplaats, $land) {
    $statement = mysqli_prepare($connection, "INSERT INTO account (voornaam, tussenvoegsel, achternaam, telefoonnummer, emailadres, wachtwoord, straat, huisnummer, postcode, woonplaats, land  ) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
    mysqli_stmt_bind_param($statement, 'sssssssssss', $voornaam, $tussenvoegsel, $achternaam, $telefoonnummer, $emailadres, $wachtwoord, $straat, $huisnummer, $postcode, $woonplaats, $land);

    mysqli_stmt_execute($statement);
    return mysqli_stmt_affected_rows($statement) == 1;
}

function passwordCheck($email, $pwd, $connection) {
    $statement = mysqli_prepare($connection, "SELECT wachtwoord, AccountID
                                                    FROM nerdygadgets.account
                                                    WHERE emailadres = ?");
    mysqli_stmt_bind_param($statement, 's', $email);
    mysqli_stmt_execute($statement);
    $result = mysqli_stmt_get_result($statement);
    $echt = mysqli_fetch_row($result);
    if(isset($echt[0])) {
        if($echt[0] === $pwd) {
            return $echt[1];
        }
    }
    return false;
}

$gegevens = array("nummer" => 0, "voornaam" => "", "tussenvoegsel" => "", "achternaam" => "", "telefoonnummer" => "","emailadres" => "", "wachtwoord" => "", "straat" => "","huisnummer" => "","postcode" => "","woonplaats" => "", "land" => "", "melding" => "");





