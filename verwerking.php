<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
include __DIR__ . "/header.php";
include "functions.php";
include "orderfuncties.php";

$connection = connectToDatabase();
$cart = getCart();
//foreach ($cart as $bestelling){
//    $id = $bestelling['id'];
//    $amount = $bestelling['amount'];
//}
$klant = $_SESSION["klant"] ?? null;  // als al wel een klantnummer stel deze dan in
//$gegevens = selecteerklant($klant); // stel de gegevens in op de gegevens uit de database van het klantnummer
$gegevens = $_SESSION["gegevens"];

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Verwerking</title>
    <style>
        h1{text-align:center;font-size:3.5em;}
        h2{text-align:center;font-size:2em;}
        hr{background-color: #00F900; border-width: 5px; height: 2px; width:60%;margin-bottom:50px;}
    </style>
</head>
<body>

<ul class="steps steps-5">
    <li><span>1. GEGEVENS</span></li>
    <li><span>2. BETAALWIJZE</span></li>
    <li><span>3. OVERZICHT</span></li>
    <li><span>4. BETALEN</span></li>
    <li class="current"><span>5. VERWERKING</span></li>
</ul>

<?php

if(isset($_POST['betalingGelukt'])){
    $orderNum = maakOrder($gegevens, $klant, $cart, $connection);
    echo "
<h1>De betaling is gelukt!</h1>
<hr>
<div class='verwerkingLinks'>
";
    echo "<h4><b>Je ordernummer: </b>".$orderNum."</h4><br>";
    echo "<b>Naam: </b><br>".$gegevens["voornaam"]." ".$gegevens["tussenvoegsel"]." ".$gegevens["achternaam"]."<br><br>";
    echo "<b>Adres: </b><br>".$gegevens["straatnaam"]." ".$gegevens["huisnummer"]."<br><br>";
    echo "<b>Woonplaats: </b><br>".$gegevens["woonplaats"]." ".$gegevens["postcode"].", ".$gegevens["land"]."<br><br>";
    echo "<b>Overige gegevens: </b><br>".$gegevens["email"]." ".$gegevens["telefoonnummer"];
    echo"
</div>

<!-- 
    mini winkelwagen laten zien, alleen foto, naam en aantal 
-->

";


} elseif(isset($_POST['betalingMislukt'])){

    echo "
    De betaling is niet gelukt. Probeer het later opnieuw!
    <form action='gegevens.php'>
        <input type='submit' value='Ga terug'>
    </form>
    ";

}

?>




