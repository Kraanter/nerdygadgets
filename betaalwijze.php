<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
//laat geen php warnings zien
//ini_set( "display_errors", 0);

include __DIR__ . "/header.php";
include "functions.php";
include "klantfuncties.php";

$connection = $databaseConnection;
$cart = getCart();
$klant = getKlant();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Betaalwijze</title>
    <style>
        h3{text-align:center;}
        .betaalLijn{border-bottom: #00F900;}
    </style>
</head>
<body>

<ul class="steps steps-5">
    <li><span>1. GEGEVENS</span></li>
    <li class="current"><span>2. BETAALWIJZE</span></li>
    <li><span>3. OVERZICHT</span></li>
    <li><span>4. BETALEN</span></li>
    <li><span>5. VERWERKING</span></li>
</ul>

<div class="betaalmethode">
    <div class="betaalKeuze">
        <div class="paypal">
            <h3>Paypal</h3>
            <hr class="betaalLijn">

            <form action="overzicht.php">
                <input type="image" name="paypal" src="Public/Img/paypal.jpg" border="0" alt="Submit" style="object-fit: contain;height: 0%;padding: 0px 20%;">
            </form>
        </div>

        <div class="ideal">
            <h3>IDeal</h3>
            <hr class="betaalLijn">
            <form method="post" action="overzicht.php">
                <select class="betalen" name="betalenIdeal" onchange="this.form.submit()">
                    <option value="" hidden selected>Kies een bank</option>
                    <option value="ing">ING</option>
                    <option value="abn">ABN AMRO</option>
                    <option value="rabobank">Rabobank</option>
                    <option value="sns">SNS</option>
                    <option value="knab">Knab</option>
                    <option value="regio">Regiobank</option>
                </select>
            </form>
        </div>

        <div class="creditcard">
            <h3>Creditcard</h3>
            <hr class="betaalLijn">
            <form method="post" action="overzicht.php">
                <select class="betalen" name="betalenCreditcard" onchange="this.form.submit()">
                    <option value="" selected hidden>Kies een credit card</option>
                    <option value="mastercard">Mastercard</option>
                    <option value="maestro">Maestro</option>
                </select>
            </form>
        </div>

        <div class="bitcoin">
            <h3>Bitcoin</h3>
            <hr class="betaalLijn">
            <form action="overzicht.php">
                <input name="bitcoin" type="image" src="Public/Img/bitcoin.png" border="0" alt="Submit" style="object-fit: contain;height:auto;padding: 0px 10%;">
            </form>
        </div>
    </div>



</div>
</body>
<?php

if(isset($_POST['gegevensOpslaan'])){
    $voornaam = $_POST['voornaam'];
    $tussenvoegsel = $_POST['tussenvoegsel'];
    $achternaam = $_POST['achternaam'];
    $telefoonnummer = $_POST['telefoonnummer'];
    $straat = $_POST['straatnaam'];
    $huisnummer = $_POST['huisnummer'];
    $postcode = $_POST['postcode'];
    $woonplaats = $_POST['woonplaats'];
    $land = $_POST['land'];
    $email = $_POST['email'];
    if(isset($_POST["wachtwoord"]) && isset($_POST["wachtwoordHerhalen"])) {
        $wachtwoord = sha1($_POST['wachtwoord']);
        $wachtwoordHerhalen = sha1($_POST['wachtwoordHerhalen']);
    } else {
        $wachtwoord = null;
        $wachtwoordHerhalen = null;
    }

    $_SESSION["gegevens"] = $_POST;

    if($email != NULL && $wachtwoord != NULL && $wachtwoordHerhalen != NULL && $klant == null) {
        if ($wachtwoord == $wachtwoordHerhalen) {
            voegKlantToe($connection, $voornaam, $tussenvoegsel, $achternaam, $telefoonnummer, $email, $wachtwoord, $straat, $huisnummer, $postcode, $woonplaats, $land);
            $_SESSION["klant"] = passwordCheck($email, $wachtwoord, $connection);
        } else {
            echo "De wachtwoorden komen niet overeen, er kan geen account aangemaakt worden.";
        }
    }

}