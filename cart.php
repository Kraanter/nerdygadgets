<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
include __DIR__ . "/header.php";
include "functions.php";

foreach ($_POST as $id => $amount) {
    $stock = getStockItem($id, $databaseConnection)["Quantity"];

    if($stock < $amount) {
        $amount = $stock;
    }
    setCartAmount($id, $amount);
}

$cart = getCart();

foreach ($cart as $id => $amount) {
    $stock = getStockItem($id, $databaseConnection)["Quantity"];
    if($amount >= 1) {
        if($stock < $amount) {
            $amount = $stock;                                           // check alle producten in de winkelwagen of ze het aantal minder is als de vooraad anders zet het aantal op de voorraad dus het maximum
        }
        setCartAmount($id, $amount);
    } else {
        remCartItem($id);
    }

}

$cart = getCart();
?>


<div id="cart" class="Browse">
    <?php
    $subtotal = 0;

    if (isset($cart) && count($cart) > 0) {

        foreach ($cart as $id => $amount) {
            /** @noinspection PhpUndefinedVariableInspection */
            $row = getStockItem($id, $databaseConnection);
            $imagepath = getStockItemImage($id, $databaseConnection)["0"] ?? Array("ImagePath" => null);
            $row["ImagePath"] = $imagepath["ImagePath"];
            if($row["ImagePath"] === null)  {
                unset($row["ImageP
                ath"]);
            }
            ?>
            <!--  coderegel 1 van User story: bekijken producten  -->


            <!-- einde coderegel 1 van User story: bekijken producten   -->
            <div id="CartFrame">
                <a  href='view.php?id=<?php print $row['StockItemID']; ?>'>
                    <?php
                    if (isset($row['ImagePath'])) { ?>
                        <div class="ImgFrame"
                             style="background-image: url('<?php print "Public/StockItemIMG/" . $row['ImagePath']; ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;"></div>
                    <?php } else if (isset($row['BackupImagePath'])) { ?>
                        <div class="ImgFrame"
                             style="background-image: url('<?php print "Public/StockGroupIMG/" . $row['BackupImagePath'] ?>'); background-size: cover;"></div>
                    <?php }
                    ?>
                </a>
                <a href='view.php?id=<?php print $row['StockItemID']; ?>' id="CartItem">
                        <h1 class="StockItemID">Artikelnummer: <?php print $row["StockItemID"]; ?></h1>
                        <p class="StockItemName"><?php print $row["StockItemName"]; ?></p>
                        <p class="StockItemComments"><?php print $row["MarketingComments"]; ?></p>
                </a>
                <a href='view.php?id=<?php print $row['StockItemID']; ?>' id="CartItem">
                    <h1 class="StockItemPriceText" id=<?php print("CartItemPrice-" . $id)?> ><?php print sprintf(" %0.2f", $row["SellPrice"]); ?></h1>
                </a>
                <div id="CartItem">
                    <h1 class="StockItemName">Aantal:<br> </h1>
                    <?php
                    $stock = $row["Quantity"];
                    $max = 0;
                    if($stock > 10){
                        $max= 10;

                    }else($max = $stock);
                    ?>

                    <form method="POST" action="cart.php">
                        <?php
                        if($amount < 10){
                            print("<select name=" . $id . " id='ItemAmountSelect-" . $id . "'  onchange='this.form.submit()'>");
                            for ($i = 1; $i <= $max; $i++) {
                                if ($i > 9) {
                                    print("<option  value='10'>" . "10+" . "</option>");
                                } else {
                                    print("<option value=' $i' ");
                                    if ($i == $amount) {
                                        print("selected");
                                    }
                                    print(">" . $i . "</option>");
                                }
                            }
                        } else {
                            print('<input type="number" 
                                    min="1"
                                    max="'. $stock .'"
                                    step="1" 
                                    class="InputAmount"
                                    name="'. $id .'" 
                                    id="ItemAmountSelect-'. $id .'" 
                                    value="'. $amount .'"
                                    required
                                    >');
                            print('<input class="InputAmountButton" type="submit" value="Updaten">');
                        }
                            ?>

                        </select>
                    </form>
                </div>
                <div id="CartItem">

                    <form method="post" action="cart.php">
                        <button type="submit" name="<?php print $id ?>" value="0" style="outline:none; color: white; font-size: 30px; background:none; border:none; padding:0;" class="" aria-hidden="true">
                            <i class="fas fa-trash"></i>
                        </button>

                </div>
                <div id="CartItem">
                    <h1 class="StockItemText">Totaal: </h1>
                        <h1 class="StockItemPriceText StockItemTotal" id=<?php print("StockItemPrice-" . $id)?>><?php $total = $amount * round($row["SellPrice"], 2);  print(sprintf("%0.2f", $total)); $subtotal+= $total?>
                    </h1>
                </div>
            </div>
                <!--  coderegel 2 van User story: bekijken producten  -->




            <!--  einde coderegel 2 van User story: bekijken producten  -->
        <?php } ?>
            <div id="TotalFrame">
                <div id="CartItem" style="grid-column-start: 5">
                    <a href="login.php" class="bestelKnop"> Bestellen</a>
                </div>
                <div id="CartItem" style="grid-column-start: 6; width: max-content">
                    <hr class="betaalLijn" style="background-color: #676EFF">
                    <h6> Verzendkosten €2,95</h6>
                    <h1 class="StockItemText">Subtotaal: </h1><h1 id="Subtotal" class="StockItemPriceText"><?php print($subtotal)?></h1>
                    <div class="BetaalMethodesProduct">
                        <a href="http://paypal.com">
                            <img href= "http://paypal.com" src="Public/Img/paypal.jpg" style="width:60px;" title="Klik om meer te weten over PayPal."> </a>
                        <a href="mollie.com">
                            <img src="Public/Img/ideal.png" style="width:50px;" title="Klik om meer te weten over iDeal."></a>
                        <a href="http://mastercard.nl">
                            <img src="Public/Img/creditcard.png" style="width:100px; " title="Klik om meer te weten over Mastercard."> </a>
                        <a href="http://bitcoin.nl">
                            <img src="Public/Img/bitcoin.png" style="width:90px;" title="Klik om meer te weten over Bitcoin."> </a>
                    </div>
                </div>
            </div>
        <?php
    } else {
        ?>
        <h2 id="NoSearchResults">
            Helaas, je winkelmand is leeg...
        </h2>
        <?php
    }
    ?>
</div>

<script>
    function updateTotaal() {
        var totaal = 0
        totaal += 2.5;
        prices = document.getElementsByClassName("StockItemTotal");
        for (let i = 0; i < prices.length; i++) {
            const price = prices[i].innerText;
            totaal += parseFloat(price);
        }
        document.getElementById("Subtotal").innerText = totaal.toFixed(2);
    }

    updateTotaal()

    function subTotal(id) {
        const price = document.getElementById("CartItemPrice-" + id);
        const select = document.getElementById("ItemAmountSelect-" + id);
        const parsedPrice = parseFloat(price.innerText);
        const quantity = parseInt(select.value);
        document.getElementById("StockItemPrice-" + id).innerText = Math.max(0, (parsedPrice * quantity)).toFixed(2).toString();

        updateTotaal();
    }
</script>

<?php
include __DIR__ . "/footer.php";
?>