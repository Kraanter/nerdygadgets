<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
include __DIR__ . "/header.php";
include "functions.php";


$cart = getCart();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Overzicht</title>
    <style>
        h1{text-align:center;}
    </style>
</head>
<body>

<ul class="steps steps-5">
    <li><span>1. GEGEVENS</span></li>
    <li><span>2. BETAALWIJZE</span></li>
    <li class="current"><span>3. OVERZICHT</span></li>
    <li><span>4. BETALEN</span></li>
    <li><span>5. VERWERKING</span></li>
</ul>

<div class="bestelling" id="overlay">
    <h1>Je bestelling is verwerkt!</h1>
    <br><br>
    <form action="betalen.php">
        <input type="submit" name="doorNaarBetalen" value="Door naar betalen!">
    </form>
    <a href="betaalwijze.php"><button style="height: 50px;width: 100%;padding: 0 10px;">Terug naar betaalwijze</button></a>
</div>

</body>
</html>

