#!/usr/bin/python3

#
# NAME
#	measure.py - script to store sense hat measurements in SQL database
#
# SYNOPSIS
#	measure.py [-v] [-t interval]
#		-v: verbose
#		-t interval: sample every interval seconds
#
# DESCRIPTION
#	measures temperature data from the raspbery pi sense hat and
#	store data in a local SQL database
#

# import some modules
import sys
import getopt
import sense_hat
import time
import mysql.connector as mariadb
from mysql.connector import errorcode

# sensor name
sensor_name = 'Warenhuis'

# database connection configuration
dbconfig = {
    'user': 'temp',
    'password': 'Pi',
    'host': '10.80.17.2',
    'database': 'nerdygadgets',
    'raise_on_warnings': True,
}

# parse arguments
verbose = True
interval = 2  # second

opts, args = getopt.getopt(sys.argv[1:], "vt:")

for opt, arg in opts:
    if opt == '-v':
        verbose = False
    elif opt == '-t':
        interval = int(arg)

# instantiate a sense-hat object
sh = sense_hat.SenseHat()

# infinite loop
while True:
    # instantiate a database connection
    mariadb_connection = mariadb.connect(**dbconfig)
    if verbose:
        print("Database connected")

    # create the database cursor for executing SQL queries
    cursor = mariadb_connection.cursor(buffered=True)

    # turn on autocommit
    cursor.autocommit = True

    # get the sensor_id for temperature sensor
    cursor.execute("SELECT id FROM sensor WHERE naam=%s", [sensor_name])

    sensor_id = cursor.fetchone()
    if sensor_id is None:
        print("Error: no sensor found with naam = %s" % sensor_name)
        sys.exit(2)

    if verbose:
        print("Reading data from sensor %s with id %s" % (sensor_name, sensor_id[0]))

    # measure temperature
    temp = round(sh.get_temperature() -30, 1)

    # verbose
    if verbose:
        print("Temperature: %s C" % temp)

    # store measurement in database
    cursor.execute('INSERT INTO meting (waarde, sensor_id) VALUES (%s, %s);', (temp, sensor_id[0]))

    # commit measurements
    mariadb_connection.commit()

    if verbose:
        print("Temperature committed")

    # close db connection
    cursor.close()
    mariadb_connection.close()
    time.sleep(interval)

# done
