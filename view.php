<!-- dit bestand bevat alle code voor de pagina die één product laat zien -->
<?php
include __DIR__ . "/header.php";
include "functions.php";

$StockItem = getStockItem($_GET['id'], $databaseConnection);
$StockItemImage = getStockItemImage($_GET['id'], $databaseConnection);
$mandje = "fa-cart-plus"; // icoon in de bestel knop

if (isset($_POST["stockItemID"])) {
    $stockItemID = $_POST["stockItemID"];
    $mandje = addProductToCart($stockItemID, $databaseConnection); // maak gebruik van geïmporteerde functie uit cartfuncties.php om het icoon voor de bestelknop te pakken
}
?>
<div id="CenteredContent">
    <?php
    if ($StockItem != null) {
        ?>
        <?php
        if (isset($StockItem['Video'])) {
            ?>
            <div id="VideoFrame">
                <?php print $StockItem['Video']; ?>
            </div>
        <?php }
        ?>


        <div id="ArticleHeader">
            <?php
            if (isset($StockItemImage)) {
                // één plaatje laten zien
                if (count($StockItemImage) == 1) {
                    ?>
                    <div id="ImageFrame"
                         style="background-image: url('Public/StockItemIMG/<?php print $StockItemImage[0]['ImagePath']; ?>'); background-size: 300px; background-repeat: no-repeat; background-position: center;"></div>
                    <?php
                } else if (count($StockItemImage) >= 2) { ?>
                    <!-- meerdere plaatjes laten zien -->
                    <div id="ImageFrame">
                        <div id="ImageCarousel" class="carousel slide" data-interval="false">
                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                                <?php for ($i = 0; $i < count($StockItemImage); $i++) {
                                    ?>
                                    <li data-target="#ImageCarousel"
                                        data-slide-to="<?php print $i ?>" <?php print (($i == 0) ? 'class="active"' : ''); ?>></li>
                                    <?php
                                } ?>
                            </ul>

                            <!-- slideshow -->
                            <div class="carousel-inner">
                                <?php for ($i = 0; $i < count($StockItemImage); $i++) {
                                    ?>
                                    <div class="carousel-item <?php print ($i == 0) ? 'active' : ''; ?>">
                                        <img src="Public/StockItemIMG/<?php print $StockItemImage[$i]['ImagePath'] ?>">
                                    </div>
                                <?php } ?>
                            </div>

                            <!-- knoppen 'vorige' en 'volgende' -->
                            <a class="carousel-control-prev" href="#ImageCarousel" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#ImageCarousel" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div id="ImageFrame"
                     style="background-image: url('Public/StockGroupIMG/<?php print $StockItem['BackupImagePath']; ?>'); background-size: cover;"></div>
                <?php
            }
            ?>


            <h1 class="StockItemID">Artikelnummer: <?php print $StockItem["StockItemID"]; ?></h1>
            <h2 class="StockItemNameViewSize StockItemName">
                <?php print $StockItem['StockItemName']; ?>
            </h2>
            <div class="QuantityText"><?php print $StockItem['QuantityOnHand']; ?></div>
            <div id="StockItemHeaderLeft">
                <div class="CenterPriceLeft">
                    <div class="CenterPriceLeftChild">
                        <p class="StockItemPriceText"><b><?php print sprintf("%.2f", $StockItem['SellPrice']); ?></b></p>
                        <h6 style="font-size: 12px"> + €2,95 verzendkosten</h6>
                        <h6 style="font-size: 12px"> Voor 23:59 besteld, Morgen in huis</h6>
                        <h6 style="font-size: 12px"> Inclusief BTW </h6>
                        <h6 style="font-size: 12px"> Garantie en Retourbeleid </h6>
                        <br><br><br><br>
                        <div class="CenterPriceAdd">
                            <?php if($StockItem["Quantity"] <= 0) {
                                print("<h6>Helaas, niet meer op voorraad</h6>");
                            } else {
                                print('
                            <form method="post" action="view.php?id='. $StockItem["StockItemID"]. '">
                                <input type="number" name="stockItemID" value="'. $StockItem["StockItemID"]. '" hidden>
                                <button id="'.$mandje .'" class="AddCart fa '. $mandje .'" type="submit" name="toevoegen"></button>');
                                if($mandje === "fa-exclamation-triangle") print("<h6>Helaas, niet genoeg voorraad om toe te voegen aan winkelmand</h6>");
                                if ($mandje === "fa-check-square") print("<h6>Toegevoegd aan winkelmand</h6>");
                            print('</form>
                            ');} ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div <?php if($StockItem["IsChillerStock"]){print('id="StockItemBox"');}?> >
            <div id="StockItemDescription">
                <h3>Artikel beschrijving</h3>
                <p><?php print $StockItem['SearchDetails']; ?></p>
            </div>
            <div id="StockItemSpecifications">
                <h3>Artikel specificaties</h3>
                <?php
                $CustomFields = json_decode($StockItem['CustomFields'], true);
                if (is_array($CustomFields)) { ?>
                    <table>
                    <thead>
                    <th>Naam</th>
                    <th>Data</th>
                    </thead>
                    <?php
                    foreach ($CustomFields as $SpecName => $SpecText) { ?>
                        <tr>
                            <td>
                                <?php print $SpecName; ?>
                            </td>
                            <td>
                                <?php
                                if (is_array($SpecText)) {
                                    foreach ($SpecText as $SubText) {
                                        print $SubText . " ";
                                    }
                                } else {
                                    print $SpecText;
                                }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </table><?php
                } else { ?>

                    <p><?php print $StockItem['CustomFields']; ?>.</p>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php if($StockItem["IsChillerStock"]){
                date_default_timezone_set("Europe/Amsterdam");
                $temp = getTemps(1, $databaseConnection);
                $secSince = time() - strtotime($temp[2]);
                print('<div id="StockItemTemp">
            <table>
                <thead>
                    <style>
        .container {
            position: relative ;
            width: 100%;
        }

        .image {
            display: block;
            width: 200%;
            height: auto;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 100%;
            right: 0;
            background-color: #21252900;
            overflow: hidden;
            width: 0;
            height: 100%;
            transition: 1s ease;
        }

        .container:hover .overlay {
            width: 100%;
            left: 0;
        }

        .text {
            color: aliceblue;
            font-size: 15px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            white-space: nowrap;
        }
    </style>
    <body>



<div class="container">
    <i  alt="Avatar" class="fas fa-snowflake"></i>
    <div class="overlay">
        <div class="text">'); print($temp[0]); print("ºC / "); print($secSince > 3600 ? " Meer dan een uur":
                                    ($secSince > 120 ? strval(floor($secSince/60)) ."  Minuten":
                                       ($secSince == 60 ? " 1 Minuut" :
                                           ($secSince > 1 ? $secSince . " Seconden" : "1 Seconde"))));

                               print(" geleden");


                print('</div>
    </div>
</div>

</body>
                    <tr>
                      
                        
                    </tr>
                    
                </thead>
            </table>
        </div>');}
    } else {
        ?><h2 id="ProductNotFound">Het opgevraagde product is niet gevonden.</h2><?php
    } ?>

</div>
