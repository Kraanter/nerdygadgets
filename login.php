<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
include __DIR__ . "/header.php";
include "functions.php";
include 'klantfuncties.php';

$connection = connectToDatabase();
$cart = getCart();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <?php



    function login($email, $pwd, $connection) {
        $inlog = passwordCheck($email, $pwd, $connection);
        if ($inlog) {
            $_SESSION["klant"] = $inlog;
            $_SESSION["gegevens"] = selecteerklant($inlog, $connection);
            return true;
        }
        return false;
    }

    $loginfout = false;

    $email = $_POST["email"] ?? "";
    $pwd = ($_POST["pwd"] ?? "") == "" ? "" : sha1($_POST["pwd"]);

    if (isset($_POST["nonLogin"])) {
        $_SESSION["gegevens"] = null;
        $_SESSION["klant"] = null;
        print("<meta http-equiv='refresh' content='0; url = gegevens.php'>");
    }

    if ($email != "" && $pwd != "") {
        if (login($email, $pwd, $connection)) {
            print("<meta http-equiv='refresh' content='0; url = gegevens.php'>");
        } else {
            $loginfout = true;
        }
    }

    if(isset($_SESSION["klant"]) && $_SESSION["klant"] > 0) {
        print("<meta http-equiv='refresh' content='0; url = gegevens.php'>");
    }
    ?>
</head>
<body>
<div class="totalLogin">

    <div class="login">
        <h2 class="loginText">Inloggen</h2>
        <form method="post" action="login.php">
            E-mail adres:<br>
            <input type="email" name="email" class="loginForm"><br>

            Wachtwoord:<br>
            <input type="password" name="pwd" class="loginForm">

            <br>
            <p style="color: red"><?php
                    if($loginfout) {
                        print("Incorrect email en/of wachtwoord");
                    }
                ?></p>
            <input type="submit" name="login" class="loginForm" value="Login">
        </form>
    </div>

    <div style="background-color: #676EFF;"></div>

    <div class="nonLogin">
        <h2 class="loginText">Als je nog geen account hebt kunt u hier bestellen als gast</h2><br>
        <form method="post" action="login.php">
            <input type="submit" name="nonLogin" value="Bestel als gast">
        </form>
    </div>
</div>
</body>
</html>


