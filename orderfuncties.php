<?php
function createOrderID($total, $databaseConnection) {
    $date = date('Y-m-d H:i:s');
    $Query = "
    INSERT INTO nerdygadgets.order(date, totalPrice)
    VALUES (?, ?)
    ";
    $Statement = mysqli_prepare($databaseConnection, $Query);
    mysqli_stmt_bind_param($Statement, "ss", $date, $total);
    mysqli_stmt_execute($Statement);

    $Query = "
                SELECT order.orderID
                FROM nerdygadgets.order 
                ORDER BY orderID DESC
                LIMIT 1";

    $Statement = mysqli_prepare($databaseConnection, $Query);
    mysqli_stmt_execute($Statement);
    $R = mysqli_stmt_get_result($Statement);
    $R = mysqli_fetch_row($R);

    return $R[0];
}

function maakOrder($gegevens, $accountID, $cart, $connection) {
    $total = 0;
    foreach($cart as $stockID => $amount) {
        $item = getStockItem($stockID, $connection);
        $total += $amount * round($item["SellPrice"], 2);
    }
    $total += 2.5;
    $orderID = createOrderID($total, $connection);
    createOrderCustomer($gegevens, $orderID, $accountID ,$connection);
    foreach($cart as $stockID => $amount) {
        createOrderline($orderID, $stockID, $amount, $connection);
        reduceStockItem($stockID, $amount, $connection);
    }
    unset($_SESSION["cart"]);
    return $orderID;
}

function createOrderCustomer($gegevens, $orderID, $accountID, $connection) {
    $statement = mysqli_prepare($connection, "INSERT INTO orderCustomer (voornaam, tussenvoegsel, achternaam, telefoonnummer, emailadres, straat, huisnummer, postcode, woonplaats, land, OrderID, AccountID  ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
    mysqli_stmt_bind_param($statement, 'ssssssssssii', $gegevens["voornaam"], $gegevens["tussenvoegsel"], $gegevens["achternaam"], $gegevens["telefoonnummer"], $gegevens["email"], $gegevens["straatnaam"], $gegevens["huisnummer"], $gegevens["postcode"], $gegevens["woonplaats"], $gegevens["land"], $orderID, $accountID);
    mysqli_stmt_execute($statement);
    return mysqli_stmt_affected_rows($statement) == 1;
}

function createOrderline($orderID, $stockID, $amount, $databaseConnection) {
    $Query = "
    INSERT INTO nerdygadgets.orderLine(orderID, stockitemID, amount)
    VALUES (?, ?, ?)
    ";

    $Statement = mysqli_prepare($databaseConnection, $Query);
    mysqli_stmt_bind_param($Statement, "iii", $orderID, $stockID, $amount);
    mysqli_stmt_execute($Statement);
}

function reduceStockItem($id, $amount, $databaseConnection) { //De voorraad van een product wordt aangepast in de database
    $Query = "
    UPDATE stockitemholdings
    SET QuantityOnHand = QuantityOnHand - ?
    WHERE StockItemID = ?
    ";

    $Statement = mysqli_prepare($databaseConnection, $Query);
    mysqli_stmt_bind_param($Statement, "ii", $amount, $id);
    mysqli_stmt_execute($Statement);
}
