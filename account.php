<?php
include __DIR__ . "/header.php";
include "functions.php";
include "databasefuncties.php";
$connection = connectToDatabase();
$cart = getCart();


?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Accountoverzicht</title>
    <link type="test/css" rel="stylesheet" href="styles.css" />
</head>

<body>
<?php if(getKlant() != null) {
    $gegevens = selecteerklant($_SESSION["klant"],$connection);?>
    <a href="./account.php">Account</a>
    <a href="./overzicht-bestellingen.php">Bestellingenoverzicht</a><br>

    <table>
        <tr>
            <th><h1>Account gegevens</h1></th>
        </tr>
        <tr>
            <td>Voornaam</td><td><?php print($gegevens["voornaam"])?></td>
        </tr>
        <tr>
            <td>Achternaam</td><td><?php print($gegevens["achternaam"])?></td>
        </tr>
        <tr>
            <td>E-mailadres</td><td><?php print($gegevens["emailadres"])?></td>
        </tr>
        <tr>
            <td>Adres</td><td><?php print($gegevens["straat"]. " " . $gegevens["huisnummer"])?></td>
        </tr>
        <tr>
            <td>Woonplaats</td><td><?php print($gegevens["woonplaats"])?></td>
        </tr>
        <tr>
            <td>Postcode</td><td><?php print($gegevens["postcode"])?></td>
        </tr>
        <tr>
            <td>Land</td><td><?php print($gegevens["land"])?></td>
        </tr>
        <tr>
            <td>Klantnummer</td><td><?php print($gegevens["AccountID"])?></td>
        </tr>

    </table>

    <?php
} else {
    print("Je bent niet ingelogd");
}?>
</body>
</html>

