
<?php

function getCart(){
    if(isset($_SESSION['cart'])){               //controleren of winkelmandje (=cart) al bestaat
        $cart = $_SESSION['cart'];                  //zo ja:  ophalen
    } else{
        $cart = array();                            //zo nee: dan een nieuwe (nog lege) array
    }
    return $cart;                               // resulterend winkelmandje terug naar aanroeper functie
}

function saveCart($cart){
    $_SESSION["cart"] = $cart;                  // werk de "gedeelde" $_SESSION["cart"] bij met de meegestuurde gegevens
}

function addProductToCart($stockItemID, $databaseConnection){
    $cart = getCart();
    $stock = getStockItem($stockItemID, $databaseConnection)["Quantity"];

    if(array_key_exists($stockItemID, $cart)){
        if($cart[$stockItemID] < $stock) {
            $cart[$stockItemID] += 1;           // voeg een product toe aan de winkelwagen als de voorraad groter is als het aantal in de winkelwagen
        } else {
            saveCart($cart);
            return "fa-exclamation-triangle";    // returnt de naam van het uitroepteken icoon als de voorraad van het product gelijk is aan het aantal in de winkelwagen
        }
    }else{
        $cart[$stockItemID] = 1;                // als er nog geen van dit product in de winkelwagen zit voeg dan dit item id toe met aantal 1
    }
    saveCart($cart);
    return "fa-check-square";                   // returnt de naam van het check icoon als er iets is toegevoegd aan de winkelwagen
}

function setCartAmount($stockItemID, $amount) {
    $cart = getCart();

    $cart[$stockItemID] = $amount;

    saveCart($cart);
}

function remCartItem($stockItemID) {
    $cart = getCart();
    unset($cart[$stockItemID]);
    saveCart($cart);
}

function getVoorraadTekst($actueleVoorraad) {
    if ($actueleVoorraad > 1000) {
        return "Ruime voorraad beschikbaar.";
    } else if($actueleVoorraad > 0){
        return "Voorraad: $actueleVoorraad";
    } else {
        return "Voorraad: 0";
    }
}
function berekenVerkoopPrijs($adviesPrijs, $btw) {
    return $btw * $adviesPrijs / 100 + $adviesPrijs;
}

function getorders($databaseConnection, $nummer){

    $Query = "select O.orderID, date, totalPrice from nerdygadgets.order O JOIN nerdygadgets.orderCustomer C ON O.orderID = C.orderID 
where AccountID = ?";

    $Statement = mysqli_prepare($databaseConnection, $Query);
    mysqli_stmt_bind_param($Statement, "i", $nummer);
    mysqli_stmt_execute($Statement);
    $Bestellingen = mysqli_stmt_get_result($Statement);
    $Result = mysqli_fetch_all($Bestellingen, MYSQLI_ASSOC);
    return $Result;

}



function getOrderTable($nummer, $databaseConnection)
{
    $Result = getorders($databaseConnection, $nummer);

    print("<table> <tr>
            <th> Ordernummer</th>
            <th> Totaal </th>
            <th> Datum </th>
            <th> Bestelling opnieuw plaatsen </th>
        </tr>");
    foreach ($Result as $order) {
        print("<tr>");
        print("<td>" . $order["orderID"] . "</td>");
        print("<td> €" . $order["totalPrice"]. "</td>");
        print('<td> ' . $order["date"] . '  </td>
              <td> <form method="post" action="Overzicht-bestellingen.php">
              <input  type="number" name="opnieuw" value="' . $order["orderID"] . '" hidden>
                      <input type="submit" value="Opnieuw bestellen"/> 
                  </form> </td>');
    }
    print("</table>");


}

function getOrderedItems($databaseConnection, $nummer){

    $Query = "select stockitemID, amount from nerdygadgets.orderLine
where orderID = ?";

    $Statement = mysqli_prepare($databaseConnection, $Query);
    mysqli_stmt_bind_param($Statement, "i", $nummer);
    mysqli_stmt_execute($Statement);
    $Bestellingen = mysqli_stmt_get_result($Statement);
    return mysqli_fetch_all($Bestellingen, MYSQLI_ASSOC);
}

function getKlant() {
    return $_SESSION["klant"] ?? null;
}


