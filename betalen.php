<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
include __DIR__ . "/header.php";
include "functions.php";


$cart = getCart();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Overzicht</title>
    <style>
        h1{text-align:center;}
    </style>
</head>
<body>

<ul class="steps steps-5">
    <li><span>1. GEGEVENS</span></li>
    <li><span>2. BETAALWIJZE</span></li>
    <li><span>3. OVERZICHT</span></li>
    <li class="current"><span>4. BETALEN</span></li>
    <li><span>5. VERWERKING</span></li>
</ul>

<form method="post" action="verwerking.php">
    <input type="submit" name="betalingGelukt" value="De betaling is correct">

</form>

<form method="post" action="verwerking.php">
    <input type="submit" name="betalingMislukt" value="De betaling is niet correct">
</form>

</body>
</html>

<?php


