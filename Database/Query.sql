
use nerdygadgets;

-- -----------------------------------------------------
-- Table `nerdygadgets`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `account` (
                           `AccountID` int(11) NOT NULL AUTO_INCREMENT,
                           `emailadres` varchar(100) NOT NULL,
                           `voornaam` varchar(100) NOT NULL,
                           `tussenvoegsel` varchar(100) DEFAULT NULL,
                           `achternaam` varchar(100) NOT NULL,
                           `straat` varchar(100) NOT NULL,
                           `huisnummer` varchar(100) NOT NULL,
                           `woonplaats` varchar(100) NOT NULL,
                           `postcode` varchar(100) NOT NULL,
                           `telefoonnummer` varchar(100) DEFAULT NULL,
                           `land` varchar(100) NOT NULL,
                           `wachtwoord` blob NOT NULL,
                           PRIMARY KEY (`AccountID`,`emailadres`),
                           UNIQUE KEY `emailadres_UNIQUE` (`emailadres`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- -----------------------------------------------------
-- Table `nerdygadgets`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nerdygadgets`.`order` (
    `orderID` INT(11) NOT NULL AUTO_INCREMENT,
    `date` DATETIME NOT NULL,
    `totalPrice` DECIMAL(6,2) NOT NULL,
    PRIMARY KEY (`orderID`))
    ENGINE = InnoDB
    AUTO_INCREMENT = 1
    DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `nerdygadgets`.`orderCustomer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nerdygadgets`.`orderCustomer` (
    `OrderID` INT NOT NULL AUTO_INCREMENT,
    `voornaam` VARCHAR(100) NOT NULL,
    `woonplaats` VARCHAR(100) NOT NULL,
    `straat` VARCHAR(100) NOT NULL,
    `huisnummer` VARCHAR(100) NOT NULL,
    `postcode` VARCHAR(100) NOT NULL,
    `achternaam` VARCHAR(100) NOT NULL,
    `telefoonnummer` VARCHAR(100) NULL DEFAULT NULL,
    `land` VARCHAR(100) NOT NULL,
    `tussenvoegsel` VARCHAR(100) NULL DEFAULT NULL,
    `emailadres` VARCHAR(100) NOT NULL,
    `AccountID` INT NULL,
    PRIMARY KEY (`OrderID`),
    INDEX `fk_orderCustomer_account_idx` (`AccountID` ASC),
    CONSTRAINT `fk_orderCustomer_account`
    FOREIGN KEY (`AccountID`)
    REFERENCES `nerdygadgets`.`account` (`AccountID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_orderCustomer_order1`
    FOREIGN KEY (`OrderID`)
    REFERENCES `nerdygadgets`.`order` (`orderID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB
    AUTO_INCREMENT = 1
    DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `nerdygadgets`.`orderLine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nerdygadgets`.`orderLine` (
    `orderlineID` INT(11) NOT NULL AUTO_INCREMENT,
    `orderID` INT(11) NOT NULL,
    `stockitemID` INT(11) NOT NULL,
    `amount` INT(11) NOT NULL,
    PRIMARY KEY (`orderlineID`, `orderID`),
    INDEX `fk_orderline_order1_idx` (`orderID` ASC),
    CONSTRAINT `fk_orderline_order1`
    FOREIGN KEY (`orderID`)
    REFERENCES `nerdygadgets`.`order` (`orderID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB
    AUTO_INCREMENT = 1
    DEFAULT CHARACTER SET = latin1;

USE `nerdygadgets`;

DELIMITER $$
CREATE
TRIGGER `nerdygadgets`.`email-orderCustomer`
BEFORE INSERT ON `nerdygadgets`.`orderCustomer`
FOR EACH ROW
BEGIN
    IF NEW.emailadres NOT LIKE '_%@_%.__%' THEN
          SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email niet juist';
END IF;
END$$

CREATE
TRIGGER `nerdygadgets`.`email-account`
BEFORE INSERT ON `nerdygadgets`.`account`
FOR EACH ROW
BEGIN
    IF NEW.emailadres NOT LIKE '_%@_%.__%' THEN
          SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email niet juist';
END IF;
END$$

CREATE
TRIGGER `nerdygadgets`.`postcode-account`
BEFORE INSERT ON `nerdygadgets`.`account`
FOR EACH ROW
BEGIN
  IF NEW.postcode NOT LIKE '[1-9][0-9][0-9][0-9][A-Z][A-Z]'
  AND NEW.postcode NOT LIKE'[1-9][0-9][0-9][0-9] [A-Z][A-Z]' THEN
       SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Postcode niet juist';
END IF;
END$$

CREATE
    TRIGGER `nerdygadgets`.`postcode-orderCustomer`
    BEFORE INSERT ON `nerdygadgets`.`orderCustomer`
    FOR EACH ROW
BEGIN
    IF NEW.postcode NOT LIKE '[1-9][0-9][0-9][0-9][A-Z][A-Z]'
  AND NEW.postcode NOT LIKE'[1-9][0-9][0-9][0-9] [A-Z][A-Z]' THEN
       SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Postcode niet juist';
END IF;
END$$

DELIMITER ;

CREATE TABLE IF NOT EXISTS sensor (
                                      id INT(11) NOT NULL AUTO_INCREMENT,
                                      naam VARCHAR(45),
                                      eenheid VARCHAR(45),
                                      PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS meting (
                                      id INT(11) NOT NULL AUTO_INCREMENT,
                                      sensor_id INT(11) NOT NULL,
                                      tijd TIMESTAMP,
                                      waarde FLOAT DEFAULT NULL,
                                      PRIMARY KEY (id),
                                      KEY fk_meting_sensor (sensor_id),
                                      CONSTRAINT fk_meting_sensor FOREIGN KEY (sensor_id) REFERENCES sensor (id)
);

INSERT INTO sensor (naam, eenheid) VALUES ('Warenhuis', 'C');
insert into meting (sensor_id, tijd, waarde) values (1, current_time(), 4.5);

CREATE USER IF NOT EXISTS 'bezoeker'@'%' IDENTIFIED BY 'Nerdygadgets!';
GRANT update, insert, select on nerdygadgets.orderLine to 'bezoeker'@'%';
GRANT update, insert, select on nerdygadgets.orderCustomer to 'bezoeker'@'%';
GRANT update, insert, select on nerdygadgets.order to 'bezoeker'@'%';
GRANT update, insert, select on nerdygadgets.account to 'bezoeker'@'%';
grant select on nerdygadgets.stockitemimages to  'bezoeker'@'%';
grant select on nerdygadgets.stockgroups to  'bezoeker'@'%';
grant select, update on nerdygadgets.stockitemholdings to  'bezoeker'@'%';
grant select on nerdygadgets.stockitems to  'bezoeker'@'%';
grant select on nerdygadgets.stockitemstockgroups to  'bezoeker'@'%';
grant select on nerdygadgets.suppliercategories to  'bezoeker'@'%';
grant select on nerdygadgets.meting to  'bezoeker'@'%';
grant select on nerdygadgets.sensor to  'bezoeker'@'%';

CREATE USER IF NOT EXISTS 'temp'@'%' IDENTIFIED BY 'Pi';
GRANT INSERT ON nerdygadgets.meting TO 'temp'@'%';
GRANT SELECT, INSERT, UPDATE ON nerdygadgets.sensor TO 'temp'@'%';
