<!-- dit bestand bevat alle code voor het productoverzicht -->
<?php
include __DIR__ . "/header.php";
include "functions.php";
include "klantfuncties.php";

$connection = connectToDatabase();
$cart = getCart();
//$klant = selecteerklant($_GET["nummer"]);

if(!isset($_SESSION["klant"])) {  // als de klant nog geen nummer heeft is het dus een gast dus klant nummer 0 voor lege gegevens
    $_SESSION["klant"] = null;
}

$klant = $_SESSION["klant"];  // als al wel een klantnummer stel deze dan in
$gegevens = selecteerklant($klant, $connection)  // stel de gegevens in op de gegevens uit de database van het klantnummer
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gegevens</title>
    <style>
        h1{text-align:center;}
        h3{text-align:center;}
        hr{background-color: #00F900; border-width: 5px; height: 2px; width:50%;}
    </style>
</head>
<body>

<ul class="steps steps-5">
    <li class="current"><span>1. GEGEVENS</span></li>
    <li><span>2. BETAALWIJZE</span></li>
    <li><span>3. OVERZICHT</span></li>
    <li><span>4. BETALEN</span></li>
    <li><span>5. VERWERKING</span></li>
</ul>

    <form method="post" class="nawGegevens gegevensContainer" action="betaalwijze.php">
        <div class="persoonGegevens">
                <input type="text" class="gegevens" name="voornaam" value="<?php print($gegevens["voornaam"]); ?>" placeholder="* Voornaam" required>
                <input type="tel" class="gegevens" name="telefoonnummer" value="<?php print($gegevens["telefoonnummer"]); ?>" placeholder="Telefoonnummer">
                <input type="text" class="gegevens" name="tussenvoegsel" value="<?php print($gegevens["tussenvoegsel"]); ?>" placeholder="Tussenvoegsel">
                <input type="text" class="gegevens" name="postcode" value="<?php print($gegevens["postcode"]); ?>" placeholder="* Postcode" required>
                <input type="text" class="gegevens" name="achternaam" value="<?php print($gegevens["achternaam"]); ?>" placeholder="* Achternaam" required>
                <input type="text" class="gegevens" name="straatnaam" value="<?php print($gegevens["straat"]); ?>" placeholder="* Straatnaam" required>
                <input type="email" class="gegevens" name="email" value="<?php print($gegevens["emailadres"]); ?>" placeholder="* E-mail adres" required>
                <input type="text" class="gegevens" name="huisnummer" value="<?php print($gegevens["huisnummer"]); ?>" placeholder="* Huisnummer" required>
                <input type="text" class="gegevens" name="woonplaats" value="<?php print($gegevens["woonplaats"]); ?>" placeholder="* Woonplaats" required>
                <input type="text" class="gegevens" name="land" value="<?php print($gegevens["land"]); ?>" placeholder="* Land" required>

                <br><br><br>
                <input type="submit" value="Verder" name="gegevensOpslaan" class="gegevensKnop">

        </div>
        <div style="background-color: #00F900"></div>
        <div class="bezorgAdres" <?php if($klant != 0){ print("hidden");} ?>>
            <label>Vul een wachtwoord in om een account aan te maken.</label>
            <input type="password" class="gegevens" name="wachtwoord" placeholder="Wachtwoord" >
            <input type="password" class="gegevens" name="wachtwoordHerhalen" placeholder="Wachtwoord herhalen" >
        </div>
    </form>
</body>
</html>

<?php


?>

